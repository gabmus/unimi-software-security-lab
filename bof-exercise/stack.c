/* stack.c */
/* This program has a buffer overflow vulnerability. */
/* Our task is to exploit this vulnerability */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
int bof(char *str)
{
	char buffer[24];
	/* The following statement has a buffer overflow problem */

	strcpy(buffer, str);
	return 1;
}

int main(int argc, char **argv)
{
	char str[517];
	FILE *myfile;
    if (argc < 2) { printf("Usage: %s FILE\n", argv[0]); return 2; }
	myfile = fopen(argv[1], "r");
	fread(str, sizeof(char), 517, myfile);
	bof(str);
	printf("Returned Properly\n");
	return 1;
}

