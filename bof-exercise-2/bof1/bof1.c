#include <stdio.h>

int main() {
    int cookie=0; // 4 byte
    char buf[80]; // 80 byte


    printf("buf: %08x cookie: %08x\n", &buf, &cookie);
    printf("cookie val: %d\n", cookie);
    gets(buf);

    if (cookie == 0x01020305)
        printf("y0u w1n!\n");
	else
		printf("nope\n");
}
